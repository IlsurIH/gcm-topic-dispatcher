package ru.burocratos.bonusrest.model.transients;

public class CloudMessage {
    String to;

    Data data;

    public CloudMessage(String to, Topic topic, double total) {
        this.to = to;
        data = new Data(topic, total);
    }

    public CloudMessage(String to, Topic topic, String message, double total) {
        this.to = to;
        data = new Data(topic, message, total);
    }

    public CloudMessage(String to, Topic topic, String message, double delta, double total) {
        this.to = to;
        data = new Data(topic, message, delta, total);
    }

    public void setTopic(Topic topic) {
        data.topic = topic;
    }

    private class Data {
        Topic topic;
        String message;
        String total;
        String delta;

        public Data(Topic topic, double total) {
            this.topic = topic;
            this.total = total + "";
        }

        public Data(Topic topic, double total, double delta) {
            this.topic = topic;
            this.total = total + "";
            this.delta = delta + "";
        }

        public Data(Topic topic, String message, double total) {
            this.topic = topic;
            this.message = message;
            this.total = total + "";
        }

        public Data(Topic topic, String message, double delta, double total) {
            this.topic = topic;
            this.message = message;
            this.delta = delta + "";
            this.total = total + "";
        }
    }
}
