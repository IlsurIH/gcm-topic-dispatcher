package ru.burocratos.bonusrest.model.transients;

public enum Topic {
    BALANCE, PAY, INVITATED, INVITATION_SUCCESS
}
