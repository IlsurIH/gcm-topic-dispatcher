package ru.burocrat.burocrat.gcm;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String topic = data.getString("topic");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some broadcast topic.
        } else {
            double total = getDouble(data.getString("total", "0.0"));
            // switch topic here
        }
    }

    protected double getDouble(String total) {
        return Double.parseDouble(total);
    }
}